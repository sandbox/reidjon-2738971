INTRODUCTION
------------

 The Slaask module integrates the Slaask chat JavaScript into your Drupal site.
 
 It requires that you to configure your own initialization key via the
 configuration page
 

 * For a full description of the module, visit the project page:
 https://www.drupal.org/sandbox/reidjon/2738971

 * To submit bug reports and feature suggestions, or to track changes:
 https://www.drupal.org/project/issues/2738971

REQUIREMENTS
------------

 This module requires no other modules.

INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module. See:
   https://drupal.org/documentation/install/modules-themes/modules-8
   for further information.

CONFIGURATION
-------------

 * Configuration is  available on the administration page:
 admin/config/slaask
 * Add your Slaask key in the widget id field and it will be injected into the
 file
   js/slaask_init.js:
 * format:
   _slaask.init('*************');

MAINTAINERS
-----------

Current maintainers:
 * Jon Reid (jonreid) - https://www.drupal.org/user/2400458

This project has been sponsored by:
 * Function1: https://www.drupal.org/function1
   We are a software company specializing in enterprise technology. Founded in
   2007, we deliver our products, world-class services, customized solutions,
   and timely support across all sectors of industry.

   Our team takes pride in delivering an amazing professional service to all of
   our clients. Whether it is product deliverables or aid in risk management we
   provide consulting services across the enterprise software spectrum.
